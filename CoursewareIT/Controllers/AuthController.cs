﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using CoursewareIT.Models;
using CoursewareIT.ViewModel;

namespace CoursewareIT.Controllers
{
    public class AuthController : Controller
    {

        // GET: Auth
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(AuthLogin request,string returnUrl)
        {
            using (var db = new CourseWareContext())
            {
                var user=db.User.FirstOrDefault(u => u.Email==request.Email);
                if (user == null || user.CheckPassword(request.Password))
                {
                    ModelState.AddModelError("Email","Email Or Password is incorrect");
                }

                if (!ModelState.IsValid)
                {
                    return View(request);
                }
                FormsAuthentication.SetAuthCookie(request.Email, true);

                if (!String.IsNullOrEmpty(returnUrl))
                    return Redirect(returnUrl);

                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Auth");
        }



        //Register: GET
        public ActionResult Register()
        {
            return View();
        }


        //Register: Post
        [HttpPost]
        public ActionResult Register(AuthRegister form)
        {
            using (var db = new CourseWareContext())
            {
                if (db.User.Any(u => u.Email == form.Email))
                {
                    ModelState.AddModelError("Email","This Email is taken please try another one :)");
                }

                if (!ModelState.IsValid)
                {
                    return View(form);
                }

                var user=new User()
                {
                    UserName = form.UserName,
                    Email = form.Email
                };

                user.SetPassword(form.Password);

                db.User.Add(user);
                db.SaveChanges();

                return RedirectToAction("Login");
            }
        }
    }
}