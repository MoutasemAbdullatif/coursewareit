﻿using System.Linq;
using System.Web.Mvc;
using CoursewareIT.Models;
using CoursewareIT.ViewModel;

namespace CoursewareIT.Controllers
{
   
    public class HomeController : Controller
    {
        
        public ActionResult Index(string searchString)
        {
            
            var db = new CourseWareContext();
            var posts = db.Post.ToList();
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                posts = posts.Where(x => x.Title.Contains(searchString)).ToList();
            }

            return View(new HomeIndex
            {
                Posts = posts
            });
        }


        public ActionResult Show(int id)
        {
            var db=new CourseWareContext();
            var post = db.Post.Find(id);
            if (post == null)
                return HttpNotFound();

            return View(post);
        }

        [Authorize]
        public ActionResult About()
        {
            return View();
        }
    }
}