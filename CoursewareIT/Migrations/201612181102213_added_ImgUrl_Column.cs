namespace CoursewareIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_ImgUrl_Column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Post", "ImgUrl", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Post", "ImgUrl");
        }
    }
}
