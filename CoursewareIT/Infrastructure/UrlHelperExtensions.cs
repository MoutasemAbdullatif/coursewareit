﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace CoursewareIT.Infrastructure
{
    public static class UrlHelperExtensions
    {
        /// <summary>
        /// Extension method to handle localized URL using a dedicated, multi-language custom route.
        /// for additional info, read the following post:
        /// http://www.ryadel.com/en/setup-a-multi-language-website-using-asp-net-mvc/
        /// </summary>
        public static string Action(
            this UrlHelper helper,
            string actionName,
            string controllerName,
            object routeValues,
            CultureInfo cultureInfo)
        {
            string localizedControllerName;
            //string routeVal = (string)routeValues;




            // fallback if cultureInfo is NULL
            if (cultureInfo == null) cultureInfo = CultureInfo.CurrentCulture;

            // arrange a "localized" controllerName to be handled with a dedicated localization-aware route.
            localizedControllerName = String.Format("{0}/{1}",
                cultureInfo.TwoLetterISOLanguageName, controllerName);

            // build the Action
            return helper.Action(
                actionName,
                controllerName,
                routeValues);
        }
    }
}