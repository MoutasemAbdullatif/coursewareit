namespace CoursewareIT.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Post")]
    public partial class Post
    {
        public Post()
        {
            Tags = new HashSet<Tag>();
        }

        [Key]
        public int PostId { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Title_tr { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Body { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Body_tr { get; set; }

        [StringLength(256)]
        public string ImgUrl { get; set; }

        [Column(TypeName = "date")]
        public DateTime? post_date { get; set; }

        public int? Views { get; set; }

        public int UserId { get; set; }

        public int? CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
