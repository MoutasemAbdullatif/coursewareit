namespace CoursewareIT.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Tag")]
    public partial class Tag
    {
        public Tag()
        {
            Posts=new HashSet<Post>();
        }
        [Key]
        public int TagId { get; set; }

        [Required]
        [StringLength(50)]
        public string TagName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
