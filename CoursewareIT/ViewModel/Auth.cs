﻿using System.ComponentModel.DataAnnotations;

namespace CoursewareIT.ViewModel
{
    public class AuthLogin
    {
        [Required,DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [DataType(DataType.Password)]
        [Required,MinLength(8)]
        public string Password { get; set; }

    }

    public class AuthRegister
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [MinLength(8),DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password",ErrorMessage = "Confirm password doesn\'t match, Type again !")]
        public string ConfirmPassword { get; set; }
    }



}