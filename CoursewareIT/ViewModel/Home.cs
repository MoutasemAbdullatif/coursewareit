﻿using System.Collections.Generic;
using CoursewareIT.Models;

namespace CoursewareIT.ViewModel
{
    public class HomeIndex
    {
        public IList<Post> Posts { get; set; }
    }

}