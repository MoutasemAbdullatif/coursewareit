﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CoursewareIT.Areas.admin.ViewModel;
using CoursewareIT.Infrastructure;
using CoursewareIT.Models;

namespace CoursewareIT.Areas.admin.Controllers
{
    [Authorize(Roles = "admin,owner")]
    [SelectedTab("users")]
    public class UsersController : Controller
    {

        // GET: admin/Users
        public ActionResult Index()
        {
            var db = new CourseWareContext();
            if (User.IsInRole("owner"))
            {
                return View(new UsersIndex()
                {
                    Users = db.User.ToList()
                });
            }
            return View(new UsersIndex()
            {
                Users = db.User.Where(u => !u.Roles.Contains(db.Role.FirstOrDefault(x => x.RoleName == "admin"))).ToList()
            });
        }


        public ActionResult EditRoles(int id)
        {
            using (var db = new CourseWareContext())
            {
                var user = db.User.Find(id);
                if (user == null)
                    return HttpNotFound();

                var roles = db.Role.Where(x=>x.RoleName!="owner").ToList();

                if (!User.IsInRole("owner"))
                {
                    roles = roles.Where(role => role.RoleName == "author" || role.RoleName == "user").ToList();
                }

                return View(new EditRoles
                {
                    Roles = roles.Select(x => new RoleCheckBox
                    {
                        Id = x.RoleId,
                        IsChecked = user.Roles.Contains(x),
                        Name = x.RoleName
                    }).ToList()
                });
            }
        }

        [HttpPost]
        public ActionResult EditRoles(int id, EditRoles form)
        {
            using (var db = new CourseWareContext())
            {
                var user = db.User.Find(id);
                var roles = user.Roles.ToList();
                var selectedRoles = new List<Role>();


                foreach (var role in db.Role.ToList())
                {
                    var checkbox = form.Roles.SingleOrDefault(c => c.Id == role.RoleId);

                    if (checkbox != null)
                    {
                        if (checkbox.IsChecked)
                        {
                            selectedRoles.Add(role);
                        }
                    }
                }

                foreach (var toAdd in selectedRoles.Where(t => !roles.Contains(t)))
                    roles.Add(toAdd);


                foreach (var toRemove in roles.Where(t => !selectedRoles.Contains(t)).ToList())
                    roles.Remove(toRemove);

                user.Roles = roles;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}