﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoursewareIT.Areas.admin.ViewModel;
using CoursewareIT.Infrastructure;
using CoursewareIT.Models;

namespace CoursewareIT.Areas.admin.Controllers
{
    [Authorize(Roles = "author,admin,owner")]
    public class PostsController : Controller
    {
        [SelectedTab("dashboard")]
        // GET: admin/Posts
        public ActionResult Index()
        {
            using (var db = new CourseWareContext())
            {
                return View(new PostsIndex()
                {
                    Posts = db.Post.ToList()
                });
            }
        }

        [SelectedTab("posts")]
        public ActionResult ViewPosts()
        {
            var db=new CourseWareContext();
            return View(new PostsViewPosts
            {
                Posts = db.Post.ToList()
            });
        }

        [SelectedTab("new")]
        public ActionResult New()
        {
            return View("Form",new PostsForm
            {
                IsNew = true
            });
        }

        [SelectedTab("new")]
        public ActionResult Edit(int id)
        {
            var db=new CourseWareContext();
            var post = db.Post.Find(id);
            if (post == null)
                return HttpNotFound();


            return View("Form",new PostsForm
            {
                IsNew = false,
                Content = post.Body,
                PostId = post.PostId,
                Title = post.Title
            });
        }

        [HttpPost]
        public ActionResult Form(PostsForm post, HttpPostedFileBase img)
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var db=new CourseWareContext())
            {
                var post = db.Post.Find(id);
                if (post==null)
                return HttpNotFound();

                db.Post.Remove(post);
                db.SaveChanges();
            }

            return RedirectToAction("ViewPosts");
        }


    }
}