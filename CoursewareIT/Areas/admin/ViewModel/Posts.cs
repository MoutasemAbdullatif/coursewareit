﻿using System.Collections;
using System.Collections.Generic;
using CoursewareIT.Models;

namespace CoursewareIT.Areas.admin.ViewModel
{
    public class PostsIndex
    { 
        public IList<Post> Posts { get; set; }
    }

    public class PostsViewPosts
    {
        public IList<Post> Posts { get; set; }
    }

    public class PostsForm
    {
        public bool IsNew { get; set; }
        public int? PostId { get; set; }


        public string Title { get; set; }
        public string Content { get; set; }
        //public IList<Tag> Tags{ get; set; }


    }
}