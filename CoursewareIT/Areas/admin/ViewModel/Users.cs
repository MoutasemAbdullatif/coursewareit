﻿using System.Collections.Generic;
using CoursewareIT.Models;

namespace CoursewareIT.Areas.admin.ViewModel
{
    public class UsersIndex
    {
        public IEnumerable<User> Users { get; set; }
    }

    public class RoleCheckBox
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Name { get; set; }
    }

    public class EditRoles
    {
        public IList<RoleCheckBox> Roles { get; set; }
    }
}